﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
public class GameInstaller : MonoInstaller<GameInstaller>
{
    public override void InstallBindings()
    {
        Container.Bind<IGameController>().To<GameController>().AsSingle().NonLazy();
        Container.Bind<IDataController>().To<DataController>().AsSingle().NonLazy();

    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class GameInit : MonoBehaviour
{
    private IGameController _gameController;


    [Inject]
    public void Setup(IGameController gameController)
    {
        _gameController = gameController;

    }

    private void Start()
    {
        Block.SetScoreAction += _gameController.SetScore;
        Block.LoseAction += _gameController.Lose;
    }



}

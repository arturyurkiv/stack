﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Init : MonoBehaviour
{
    private AsyncOperation asyncLoadScene;

    private void Start()
    {
        StartCoroutine(InitCor());
    }

    private IEnumerator InitCor()
    {

        asyncLoadScene = SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive);

        while (!asyncLoadScene.isDone)
        {
            yield return new WaitForEndOfFrame();
        }

        SceneManager.SetActiveScene(SceneManager.GetSceneByName("Game"));
        yield return null;

        Debug.Log($"[INIT DONE]__________Time = {Time.time}");

        SceneManager.UnloadSceneAsync(0);
    }

}

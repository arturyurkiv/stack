﻿
public interface IGameController
{
    bool IsPlay { get; set; }

    void Init();
    void Play();
    void Lose();
    void Restart();
    void Exit();
    void SetScore();
}

﻿public enum Direction
{
    Unknown = 0,
    Forward = 1,
    Right = 2
}


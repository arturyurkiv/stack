﻿public enum SceneType
{
    Unknown = -1,
    Init = 0,
    Game = 1
}

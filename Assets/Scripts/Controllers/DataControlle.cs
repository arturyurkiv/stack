﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataController : IDataController
{
    private int _score;

    public int SetScore
    {
        get
        {
            return PlayerPrefs.GetInt("Score");

        }

        set
        {
            if( value > PlayerPrefs.GetInt("Score") )
            {
                PlayerPrefs.SetInt("Score",value);
            }

        }

    }

}

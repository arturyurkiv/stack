﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Zenject;

public class UIController : MonoBehaviour
{
    [SerializeField] private GameObject _menuPanel;
    [SerializeField] private GameObject _gamePanel;
    [SerializeField] private GameObject _losePanel;

    [SerializeField] private TextMeshProUGUI _score;
    [SerializeField] private TextMeshProUGUI _bestScore;

    private IGameController _gameController;
    private IDataController _dataController;

    [Inject]
    public void Setup(IGameController gameController, IDataController dataController)
    {
        _gameController = gameController;
        _dataController = dataController;
    }

    private void Start()
    {
        SetBestScore();
    }

    public void ShowMenu()
    {
        _menuPanel.SetActive(true);
        SetBestScore();
    }

    /// <summary>
    /// Close menu Panel.
    /// </summary>
    public void CloseMenu()
    {
        _menuPanel.SetActive(false);
    }

    /// <summary>
    /// Open game panel. 
    /// </summary>
    public void ShowGame()
    {
        _gamePanel.SetActive(true);
    }

    /// <summary>
    /// Close game Panel.
    /// </summary>
    public void CloseGame()
    {
        _gamePanel.SetActive(false);
    }

    public void ShowLosePanel()
    {
        _losePanel.SetActive(true);
    }

    public void CloseLosePanel()
    {
        _losePanel.SetActive(false);
    }

    public void OnButtonStartClick()
    { 
        Debug.Log("Button Start Click");
        _gameController.Play();
        CloseMenu();
        ShowGame();
      
    }

    public void OnButtonRestartClick()
    {
        Debug.Log("Button Restart Click");
        CloseLosePanel();
        _gameController.Restart();
    }

    public void OnButtonExitClick()
    {
        Debug.Log("Button Exit Click");
        _gameController.Exit();
    }

    public void SetBestScore()
    {
        _bestScore.text = "Best score " + _dataController.SetScore;
    }

    public void SetScore(string score)
    {
        Debug.Log("Score");
        _score.text = score;
    }



}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

public class GameController :  IGameController
{
   public bool IsPlay { get; set; }

    private int _score;
   

    [Inject] private UIController _uiController;

    private IDataController _dataController;

    [Inject]
    public void Setup(IDataController dataController)
    {
        _dataController = dataController;
    }

    public void Init()
    {
        _score = 0;
        IsPlay = false;
    }
   
    public void Play()
    {
        IsPlay = true;
    }

    public void Lose()
    {
         IsPlay = false;
        _uiController.CloseGame();
        _uiController.ShowLosePanel();
        CameraLoseScale();
    }

    public void CameraLoseScale()
    {
        var camPosition = Camera.main.transform.position;
        Camera.main.transform.position = new Vector3(camPosition.x - _score / 2, camPosition.y , camPosition.z - _score / 2);

    }

    public void Restart()
    {
        IsPlay = true;
        SceneManager.LoadScene((int)SceneType.Game);
    }

    public void SetScore()
    {
        _score++;
        _uiController.SetScore(_score.ToString());
        _dataController.SetScore = _score;
    }

    public void Exit()
    {
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#else
       Application.Quit();

#endif
    }

}

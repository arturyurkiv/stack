﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class CameraController : MonoBehaviour
{
   private IGameController _gameController;

    [Inject]
    public void Setup(IGameController gameController)
    {
        _gameController = gameController;
    }

    private void Update()
    {
        CameraMovement(); 
    }

    private void CameraMovement()
    {
        if (Input.GetMouseButtonDown(0) && _gameController.IsPlay)
        {
            transform.position += new Vector3(0, 0.2f, 0);
        }
    }
      
}

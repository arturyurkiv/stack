﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class BlockController : MonoBehaviour
{
    private BlockSpawner[] _blockSpawners;

    private int _SpownIndex;

    private IGameController _gameController;

    [Inject]
    public void Setup(IGameController gameController)
    {
        _gameController = gameController;
    }

    private void Start()
    {
        _blockSpawners = FindObjectsOfType<BlockSpawner>();
    }

    private void Update()
    {
        OnKlick();
    }

    private void OnKlick()
    { 
        if (Input.GetMouseButtonDown(0) && _gameController.IsPlay)
        {
            if (Block.CurrentBlock != null)
            {
                Block.CurrentBlock.StopBlock();
            }

            _SpownIndex = _SpownIndex == 0 ? 1 : 0;
            _blockSpawners[_SpownIndex].Spown();
        }
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;
using UnityEngine.Events;

public class Block : MonoBehaviour
{
    public static Block CurrentBlock { get; private set;}
    public static Block LastBlock { get; private set; }

    public Direction MoveDirection { get; set; }
    public static UnityAction SetScoreAction;
    public static UnityAction LoseAction;

    [SerializeField] private bool _moveBlock;
    [SerializeField] private float _speed = 1f;

    private Block _startBlock;

    

    private void OnEnable()
    {
        _startBlock = FindObjectOfType<Block>();

        if (LastBlock == null)
        {
            LastBlock = _startBlock;
        }

        CurrentBlock = this;
        _moveBlock = true;

        transform.localScale = new Vector3(LastBlock.transform.localScale.x,
            transform.localScale.y, LastBlock.transform.localScale.z);

        GetComponent<Renderer>().material.color = GetColor();
    }

    private void OnDisable()
    {
        CurrentBlock = null;
        LastBlock = null;
    }

  
    private void FixedUpdate()
    {
        if (!_moveBlock)
           return;

        MoveBlock();    
    }

    /// <summary>
    /// Move block.
    /// 
    /// </summary>
    private void MoveBlock()
    {

        if (MoveDirection == Direction.Forward)
        {
            transform.position += transform.forward * - _speed * Time.deltaTime;     
        }
        else
        {
            transform.position += transform.right * - _speed * Time.deltaTime;
        }
    }

    public void StopBlock()
    {
        _moveBlock = false;
       
        var holdover = GetHoldover();

        if (CheckLoose(holdover))
        {
            gameObject.AddComponent<Rigidbody>();
            LoseAction.Invoke();
        }
        else if (LastBlock != _startBlock)
        { 
            var direction = holdover > 0 ? 1f : -1f;
            SliceBlock(holdover, direction);
            LastBlock = this;
            SetScoreAction.Invoke();
        }
  
    }

    private bool CheckLoose(float holdover)
    {
        float max = MoveDirection == Direction.Forward ? LastBlock.transform.localScale.z : LastBlock.transform.localScale.x;
        if (Mathf.Abs(holdover) >= max)
        {
            LastBlock = null;
            CurrentBlock = null;
            return true;
        }
        else
           return false;
    }

    private float GetHoldover()
    {
        if (MoveDirection == Direction.Forward)
        {
            return transform.position.z - LastBlock.transform.position.z;
        }
        else
        {
            return transform.position.x - LastBlock.transform.position.x;
        }
    }

    private void SliceBlock(float holdover,float direction)
    {
        if(MoveDirection == Direction.Forward)
        {
            var newSize = LastBlock.transform.localScale.z - Mathf.Abs(holdover);
            var folingBlockSize = transform.localScale.z - newSize;

            var positionZ = LastBlock.transform.position.z + (holdover / 2f);

            transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, newSize);
            transform.position = new Vector3(transform.position.x, transform.position.y, positionZ);

            var cubeEdge = transform.position.z + (newSize / 2f * direction);
            var cubePositionZ = cubeEdge + folingBlockSize / 2f * direction;

            InstantiateDropBlock(cubePositionZ, folingBlockSize);

        }
        else if (MoveDirection == Direction.Right)
        {
            var newSize = LastBlock.transform.localScale.x - Mathf.Abs(holdover);
            var folingBlockSize = transform.localScale.x - newSize;

            var positionX = LastBlock.transform.position.x + (holdover / 2f);

            transform.localScale = new Vector3(newSize,transform.localScale.y, transform.localScale.z);
            transform.position = new Vector3(positionX,transform.position.y, transform.position.z);

            var cubeEdge = transform.position.x + (newSize / 2f * direction);
            var cubePositionZ = cubeEdge + folingBlockSize / 2f * direction;


            InstantiateDropBlock(cubePositionZ, folingBlockSize);


        }

    }

    private void InstantiateDropBlock(float position, float size)
    {
        var dropCube = GameObject.CreatePrimitive(PrimitiveType.Cube);

        if (MoveDirection == Direction.Forward)
        {
            dropCube.transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, size);
            dropCube.transform.position = new Vector3(transform.position.x, transform.position.y, position);
        }
        else
        {
            dropCube.transform.localScale = new Vector3(size,transform.localScale.y, transform.localScale.z);
            dropCube.transform.position = new Vector3(position,transform.position.y, transform.position.z);
        }
        dropCube.AddComponent<Rigidbody>();
        dropCube.GetComponent<Renderer>().material.color = GetComponent<Renderer>().material.color;
    }

    private Color GetColor()
    {
        return new Color(Random.Range(0, 1f), Random.Range(0, 1f), Random.Range(0, 1f));
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class BlockSpawner : MonoBehaviour
{
    [SerializeField] private GameObject _blockPrefab;
    [SerializeField] private Direction _moveDirection;

    private IGameController _gameController;


    [Inject]
    public void Setup(IGameController gameController)
    {
        _gameController = gameController;

    }

    public void Spown()
    {
        if (!_gameController.IsPlay)
            return;

        var cube = Instantiate(_blockPrefab,transform.position,Quaternion.identity, transform);

        if(Block.LastBlock != null && Block.LastBlock.gameObject != GameObject.Find("StartCube") )
        {
            var x = _moveDirection == Direction.Right ? transform.position.x : Block.LastBlock.transform.position.x;
            var z = _moveDirection == Direction.Forward ? transform.position.z : Block.LastBlock.transform.position.z;

            cube.transform.position = new Vector3(x,Block.LastBlock.transform.position.y + _blockPrefab.transform.localScale.y,z);
        }
        else
        {
            cube.transform.position = transform.position;
        }

        cube.GetComponent<Block>().MoveDirection = _moveDirection;

    }

}
